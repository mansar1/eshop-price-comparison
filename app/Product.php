<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'eshops_product';
  public $timestamps = false;

  /**
  * Get the eshops products for the product.
  */
  public function eshops_products()
  {
    return $this->hasMany('App\EshopProduct', 'product_id');
  }

  /**
  * Get the category that owns the product.
  */
  public function category()
  {
    return $this->belongsTo('App\Category', 'category');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($product) { // before delete() method call this
         $product->eshops_products()->each(function($eshop_product) {
            $eshop_product->delete();
         });
    });
  }
}
