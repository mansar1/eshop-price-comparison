<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EshopProduct extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'eshops_eshop_product';

  /**
  * Get the product that owns the eshop product.
  */
  public function product()
  {
    return $this->belongsTo('App\Product', 'product_id');
  }

  /**
  * Get the eshop that owns the eshop product.
  */
  public function eshop()
  {
    return $this->belongsTo('App\Eshop', 'eshop_id');
  }

  /**
  * Get the coupons for the product.
  */
  public function coupons()
  {
    return $this->hasMany('App\Coupon', 'product_id');
  }

  public function getPriceHistoryAttribute($value)
  {
    return unserialize($value);
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($eshop_product) { // before delete() method call this
         $eshop_product->coupons()->each(function($coupon) {
            $coupon->delete();
         });
    });
  }
}
