<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(255);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      // ESHOP
      $this->app->singleton('geteshops', function () {
          return new GetEshops;
      });
      $this->app->singleton('geteshop', function () {
          return new GetEshop;
      });
      $this->app->singleton('storeeshop', function () {
          return new StoreEshop;
      });
      $this->app->singleton('deleteeshop', function () {
          return new DeleteEshop;
      });

      // PRODUCT
      $this->app->singleton('getproducts', function () {
          return new GetProducts;
      });
      $this->app->singleton('getproduct', function () {
          return new GetProduct;
      });
      $this->app->singleton('storeproduct', function () {
          return new StoreProduct;
      });
      $this->app->singleton('deleteproduct', function () {
          return new DeleteProduct;
      });

      // ESHOP PRODUCT
      $this->app->singleton('geteshopproducts', function () {
          return new GetEshopProducts;
      });
      $this->app->singleton('geteshopproduct', function () {
          return new GetEshopProduct;
      });
      $this->app->singleton('storeeshopproduct', function () {
          return new StoreEshopProduct;
      });
      $this->app->singleton('deleteeshopproduct', function () {
          return new DeleteEshopProduct;
      });
      $this->app->singleton('findoriginalprice', function () {
          return new FindOriginalPrice;
      });

      // CATEGORY
      $this->app->singleton('getcategories', function () {
          return new GetCategories;
      });
      $this->app->singleton('getcategory', function () {
          return new GetCategory;
      });
      $this->app->singleton('storecategory', function () {
          return new StoreCategory;
      });
      $this->app->singleton('deletecategory', function () {
          return new DeleteCategory;
      });
      $this->app->singleton('getcategorybranch', function () {
          return new GetCategoryBranch;
      });
      $this->app->singleton('getcategorybranches', function () {
          return new GetCategoryBranches;
      });

      // COUPON
      $this->app->singleton('getcoupons', function () {
          return new GetCoupons;
      });
      $this->app->singleton('getcoupon', function () {
          return new GetCoupon;
      });
      $this->app->singleton('storecoupon', function () {
          return new StoreCoupon;
      });
      $this->app->singleton('deletecoupon', function () {
          return new DeleteCoupon;
      });
    }
}
