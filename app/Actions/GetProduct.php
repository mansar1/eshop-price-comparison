<?php
namespace App\Actions;

use App\Product;

class GetProduct
{
  public function execute(int $id) : Product
  {
    return  Product::with(['eshops_products', 'eshops_products.coupons'])
                      ->findOrFail($id);
  }
}


?>
