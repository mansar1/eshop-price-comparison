<?php
namespace App\Actions;

use Validator;
use App\Product;

class StoreProduct
{
  protected $getProduct;

  public function __construct(GetProduct $getProduct)
  {
    $this->getProduct = $getProduct;
  }

  public function execute(array $data) : Product
  {
    // validate data
    $this->validate($data);

    // get existing or create new product
    $product = isset($data['id']) ? $this->getProduct->execute($data['id']) : new Product;

    if(isset($data['name']))
      $product->name = $data['name'];

    if(isset($data['property']))
      $product->property = $data['property'];
    else if(array_key_exists('property', $data))
      $product->property = NULL;

    if(isset($data['image_url']))
      $product->image_url = $data['image_url'];

    if(isset($data['thumbnail_image_url']))
      $product->thumbnail_image_url = $data['thumbnail_image_url'];
    else if(array_key_exists('thumbnail_image_url', $data))
      $product->thumbnail_image_url = NULL;

    if(isset($data['category']))
      $product->category = $data['category'];

    if(isset($data['rating_value']))
      $product->rating_value = $data['rating_value'];
    else if(array_key_exists('rating_value', $data))
      $product->rating_value = NULL;

    if(isset($data['rating_count']))
      $product->rating_count = $data['rating_count'];
    else if(array_key_exists('rating_count', $data))
      $product->rating_count = NULL;

    if(isset($data['is_vip']))
      $product->is_vip = $data['is_vip'];

    if(isset($data['group_id']))
      $product->group_id = $data['group_id'];
    else if(array_key_exists('group_id', $data))
      $product->group_id = NULL;

    $product->save();

    return $product;
  }

  private function validate(array $data)
  {
    if(isset($data['id']))
    {
      Validator::make($data, [
        'name' => 'string|min:3|max:255',
        'property' => 'string|min:2|max:255|nullable',
        'image_url' => 'string|min:3|max:255|url',
        'thumbnail_image_url' => 'string|min:3|max:255|nullable|url',
        'category' => 'integer|exists:eshops_category,id',
        'rating_value' => 'nullable|numeric',
        'rating_count' => 'nullable|integer',
        'is_vip' => 'boolean',
        'group_id' => 'nullable|integer',
      ])->validate();
    }
    else
    {
      Validator::make($data, [
        'name' => 'required|string|min:3|max:255',
        'property' => 'string|min:2|max:255|nullable',
        'image_url' => 'string|min:3|max:255|url',
        'thumbnail_image_url' => 'string|min:3|max:255|nullable|url',
        'category' => 'required|integer|exists:eshops_category,id',
        'rating_value' => 'nullable|numeric',
        'rating_count' => 'nullable|integer',
        'is_vip' => 'boolean',
        'group_id' => 'nullable|integer',
      ])->validate();
    }
  }
}


?>
