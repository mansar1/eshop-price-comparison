<?php
namespace App\Actions;

class DeleteCoupon
{
  protected $getCoupon;

  public function __construct(GetCoupon $getCoupon)
  {
    $this->getCoupon = $getCoupon;
  }

  public function execute(int $id) : bool
  {
    // get coupon
    $coupon = $this->getCoupon->execute($id);

    return $coupon->delete();
  }
}


?>
