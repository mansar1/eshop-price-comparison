<?php
namespace App\Actions;

use App\Eshop;

class GetEshop
{
  public function execute(int $id) : Eshop
  {
    return  Eshop::findOrFail($id);
  }
}


?>
