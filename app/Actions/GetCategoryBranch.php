<?php
namespace App\Actions;

use Validator;
use App\Category;

class GetCategoryBranch
{
  private $only_vip;
  private $not_empty;

  public function __construct()
  {
    $this->only_vip = false;
    $this->not_empty = false;
  }

  /**
   * Get category tree - every parent and child
   * @param  object  $category      start node
   * @param  array $data additional parameters
   * @return object                 category branch
   */
  public function execute(Category $category, array $data) : Category
  {
    // validate data
    $this->validate($data);

    $this->only_vip = isset($data['only_vip']) ? $data['only_vip'] : false;
    $this->not_empty = isset($data['not_empty']) ? $data['not_empty'] : false;

    // get children
    $category->subcats = !isset($category->eshop_id) ?
        $category->children()->get()
        :
        $category->eshop_children()->get();

    $category->products_count = 0;
    foreach ($category->subcats as $key => $child) {
        $child->subcats = $this->getChildren($child);
        $category->products_count += $child->products_count;
        if($this->not_empty && $child->products_count < 1){
          unset($category->subcats[$key]);
        }
    }

    $category->subcats = $category->subcats->values();

    // get parents
    if(isset($data['only_children']) && $data['only_children'])
    {
      if(!isset($category->eshop_id)){
        while($category->parent_id){
          $temp_cat = $category;
          $category = $temp_cat->parent;
          $category->subcats = $temp_cat;
        }
      }
      else{
        while($category->eshop_parent_id){
          $temp_cat = clone $category;
          $category = new Category();
          $category = clone $temp_cat->eshop_parent()->first();
          $category->subcats = clone $temp_cat;
        }
      }
    }

    return $category;
  }

  private function validate(array $data)
  {
    Validator::make($data, [
        'only_children' => 'boolean',
        'not_empty' => 'boolean',
        'only_vip' => 'boolean',
      ])->validate();
  }

  /**
   * Recursive method for children search
   * @param  Collection $parent start node
   * @return Collection          children tree
   */
  private function getChildren(object $parent) : object
  {
    if(!$parent->eshop_id){
      $parent->subcats = $parent->children()->withCount(['products' => function ($query) {
          if($this->only_vip == true)
            $query->where('is_vip', true);
      }])->get();
    }
    else{
      $parent->subcats = $parent->eshop_children()->withCount(['products' => function ($query) {
          if($this->only_vip == true)
            $query->where('is_vip', true);
      }])->get();
    }

    $parent->products_count = isset($parent->products_count) ? $parent->products_count : 0;

    foreach ($parent->subcats as $key => $child) {
      $parent->products_count += $child->products_count;
      if($child->products_count < 1)
        $child->subcats = $this->getChildren($child);

      if($this->not_empty && $child->products_count < 1){
        unset($parent->subcats[$key]);
      }
    }
    return $parent->subcats->values();
  }
}


?>
