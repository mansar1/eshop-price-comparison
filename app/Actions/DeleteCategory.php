<?php
namespace App\Actions;

class DeleteCategory
{
  protected $getCategory;

  public function __construct(GetCategory $getCategory)
  {
    $this->getCategory = $getCategory;
  }

  public function execute(int $id) : bool
  {
    // get category
    $category = $this->getCategory->execute($id);

    return $category->delete();
  }
}


?>
