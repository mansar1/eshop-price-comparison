<?php
namespace App\Actions;

class DeleteProduct
{
  protected $getProduct;

  public function __construct(GetProduct $getProduct)
  {
    $this->getProduct = $getProduct;
  }

  public function execute(int $id) : bool
  {
    // get product
    $product = $this->getProduct->execute($id);

    return $product->delete();
  }
}


?>
