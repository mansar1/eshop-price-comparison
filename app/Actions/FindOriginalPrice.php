<?php
namespace App\Actions;

class FindOriginalPrice
{
  /**
   * Count original price according to price history.
   *
   * @param  array  $priceHistory
   * @return float $original_price
   */
  public function execute(array $priceHistory) : float
  {
    $pricesSum = 0;
    $pricesCount = 0;
    $tempInc = false;
    $tempDec = false;
    $maxDays = 10;
    $previousRecord = false;

    for ($i=0; $i < count($priceHistory); $i++) {
      $record = $priceHistory[$i];

      if($i > 0){
        //ignore temporary increases
        if($record['price'] > $previousRecord['price']){
          $tempInc = false;
          for ($j=$i; $j < $i+$maxDays && $j < count($priceHistory); $j++) {
            if($previousRecord['price'] == $priceHistory[$j]['price']){
              $i = $j-1;
              $tempInc = true;
            }
          }

          if(!$tempInc){
            $pricesSum += $record['price'];
            $pricesCount++;
          }
        //ignore temporary decreases
        }else if($record['price'] < $previousRecord['price']){
          $tempDec = false;
          for ($j=$i; $j < $i+$maxDays && $j < count($priceHistory); $j++) {
            if($previousRecord['price'] == $priceHistory[$j]['price']){
              $i = $j-1;
              $tempDec = true;
            }
          }

          if(!$tempDec){
            $pricesSum += $record['price'];
            $pricesCount++;
          }
        }else{
          $pricesSum += $record['price'];
          $pricesCount++;
        }
      }else{
        $pricesSum += $record['price'];
        $pricesCount++;
      }

      if($tempDec || $tempInc)
        $previousRecord = $priceHistory[$i+1];
      else
        $previousRecord = $priceHistory[$i];

    }

    return round($pricesSum / $pricesCount, 2);
  }
}


?>
