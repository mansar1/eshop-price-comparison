<?php
namespace App\Actions;

class DeleteEshop
{
  protected $getProduct;

  public function __construct(GetEshop $getEshop)
  {
    $this->getEshop = $getEshop;
  }

  public function execute(int $id) : bool
  {
    // get eshop
    $eshop = $this->getEshop->execute($id);

    return $eshop->delete();
  }
}


?>
