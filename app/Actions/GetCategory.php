<?php
namespace App\Actions;

use App\Category;

class GetCategory
{
  public function execute(int $id) : Category
  {
    return  Category::findOrFail($id);
  }
}


?>
