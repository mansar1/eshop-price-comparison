<?php
namespace App\Actions;

use App\Coupon;

class GetCoupons
{
  public function execute(int $limit)
  {
    return Coupon::with(['eshop_product'])->paginate($limit);
  }
}


?>
