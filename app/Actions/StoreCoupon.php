<?php
namespace App\Actions;

use Validator;
use Illuminate\Validation\Rule;
use App\Coupon;

class StoreCoupon
{
  protected $getCoupon;

  public function __construct(GetCoupon $getCoupon)
  {
    $this->getCoupon = $getCoupon;
  }

  public function execute(array $data) : Coupon
  {
    // validate data
    $this->validate($data);

    // get existing or create new product
    $coupon = isset($data['id']) ? $this->getCoupon->execute($data['id']) : new Coupon;

    if(isset($data['name']))
      $coupon->name = $data['name'];

    if(isset($data['product_id']))
      $coupon->product_id = $data['product_id'];
    else if(array_key_exists('product_id', $data))
      $coupon->product_id = NULL;

    if(isset($data['eshop_id']))
      $coupon->eshop_id = $data['eshop_id'];
    else if(array_key_exists('eshop_id', $data))
      $coupon->eshop_id = NULL;

    if(isset($data['image_url']))
      $coupon->image_url = $data['image_url'];
    else if(array_key_exists('image_url', $data))
      $coupon->image_url = NULL;

    if(isset($data['category']))
      $coupon->category = $data['category'];

    if(isset($data['description']))
      $coupon->description = $data['description'];
    else if(array_key_exists('description', $data))
      $coupon->description = NULL;

    if(isset($data['type']))
      $coupon->type = $data['type'];

    if(isset($data['code']))
      $coupon->code = $data['code'];

    if(isset($data['url']))
      $coupon->url = $data['url'];

    if(isset($data['promotion_url']))
      $coupon->promotion_url = $data['promotion_url'];
    else if(array_key_exists('promotion_url', $data))
      $coupon->promotion_url = NULL;

    if(isset($data['max_limit']))
      $coupon->max_limit = $data['max_limit'];
    else if(array_key_exists('max_limit', $data))
      $coupon->max_limit = NULL;

    if(isset($data['current_used']))
      $coupon->current_used = $data['current_used'];
    else if(array_key_exists('current_used', $data))
      $coupon->current_used = NULL;

    if(isset($data['discount']))
      $coupon->discount = $data['discount'];

    if(isset($data['discount_type']))
      $coupon->discount_type = $data['discount_type'];

    if(isset($data['valid_from']))
      $coupon->valid_from = $data['valid_from'];

    if(isset($data['valid_to']))
      $coupon->valid_to = $data['valid_to'];

    $coupon->save();

    return $coupon;
  }

  private function validate(array $data)
  {
    if(isset($data['id']))
    {
      Validator::make($data, [
        'name' => 'string|min:3|max:255',
        'product_id' => 'integer|exists:eshops_eshop_product,id|nullable',
        'eshop_id' => 'integer|exists:eshops_eshop,id|nullable',
        'description' => 'string|nullable',
        'url' => 'string|min:3|max:255|url',
        'promotion_url' => 'string|min:3|max:255|url',
        'image_url' => 'string|min:3|max:255|url',
        'category' => 'integer|exists:eshops_category,id',
        'type' => 'string|max:255',
        'code' => 'string|max:255',
        'max_limit' => 'nullable|integer',
        'current_used' => 'nullable|integer',
        'discount' => 'numeric',
        'discount_type' => Rule::in(['percent', 'price']),
        'valid_from' => 'date',
        'valid_to' => 'date',
      ])->validate();
    }
    else
    {
      Validator::make($data, [
        'name' => 'required|string|min:3|max:255',
        'product_id' => 'integer|exists:eshops_eshop_product,id|nullable',
        'eshop_id' => 'integer|exists:eshops_eshop,id|nullable',
        'description' => 'string|nullable',
        'url' => 'required|string|min:3|max:255|url',
        'promotion_url' => 'string|min:3|max:255|url',
        'image_url' => 'string|min:3|max:255|url',
        'category' => 'required|integer|exists:eshops_category,id',
        'type' => 'required|string|max:255',
        'code' => 'required|string|max:255',
        'max_limit' => 'nullable|integer',
        'current_used' => 'nullable|integer',
        'discount' => 'required|numeric',
        'discount_type' => ['required', Rule::in(['percent', 'price'])],
        'valid_from' => 'required|date',
        'valid_to' => 'required|date',
      ])->validate();
    }
  }
}


?>
