<?php
namespace App\Actions;

use App\Product;

class GetProducts
{
  public function execute(int $limit)
  {
    return Product::with(['eshops_products', 'eshops_products.coupons'])->paginate($limit);
  }
}


?>
