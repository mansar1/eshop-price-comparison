<?php
namespace App\Actions;

use App\EshopProduct;
use Illuminate\Support\Facades\DB;

class GetEshopProduct
{
  public function execute(int $id) : EshopProduct
  {

    return  EshopProduct::
            select('*', DB::raw('date(sale_endtime) as sale_endtime'),
                     DB::raw('ROUND((current_price - lowest_price), 2) AS diff_lowest_price'),
                     DB::raw('ROUND((original_price - current_price), 2) AS absolute_decrease'),
                     DB::raw('ROUND((100 - current_price * 100 / original_price), 2) AS relative_decrease'))
            ->with(['product', 'eshop', 'coupons'])->findOrFail($id);
  }
}


?>
