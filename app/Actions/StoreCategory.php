<?php
namespace App\Actions;

use Validator;
use App\Category;

class StoreCategory
{
  protected $getCategory;

  public function __construct(GetCategory $getCategory)
  {
    $this->getCategory = $getCategory;
  }

  public function execute(array $data) : Category
  {
    // validate data
    $this->validate($data);

    // get existing or create new category
    $category = isset($data['id']) > 0 ? $this->getCategory->execute($data['id']) : new Category;

    if(isset($data['name']))
      $category->name = $data['name'];

    if(isset($data['eshop_id']))
      $category->eshop_id = $data['eshop_id'];
    else if(array_key_exists('eshop_id', $data))
      $category->eshop_id = NULL;

    if(isset($data['parent_id']) && !$category->eshop_id)
      $category->parent_id = $data['parent_id'];
    else if(array_key_exists('parent_id', $data))
      $category->parent_id = NULL;

    if($category->eshop_id){
      $category->parent_id = NULL;

      if(isset($data['eshop_cat_id']))
        $category->eshop_cat_id = $data['eshop_cat_id'];
      else if(array_key_exists('eshop_cat_id', $data))
        $category->eshop_cat_id = NULL;

      if(isset($data['eshop_parent_id']))
        $category->eshop_parent_id = $data['eshop_parent_id'];
      else if(isset($data['eshop_parent_id']) || array_key_exists('eshop_parent_id', $data))
        $category->eshop_parent_id = NULL;
    }
    else{
      $category->eshop_cat_id = NULL;
      $category->eshop_parent_id = NULL;
    }

    $category->save();

    return $category;
  }

  private function validate(array $data)
  {
    if(isset($data['id']))
    {
      Validator::make($data, [
        'name' => 'string|min:3|max:255',
        'parent_id' => 'nullable|integer|exists:eshops_category,id',
        'eshop_id' => 'nullable|integer|exists:eshops_eshop,id',
        'eshop_cat_id' => 'nullable|integer',
        'eshop_parent_id' => 'nullable|integer|exists:eshops_category,eshop_cat_id',
      ])->validate();
    }
    else
    {
      Validator::make($data, [
        'name' => 'required|string|min:3|max:255',
        'parent_id' => 'nullable|integer|exists:eshops_category,id',
        'eshop_id' => 'nullable|integer|exists:eshops_eshop,id|required_with:eshop_cat_id',
        'eshop_cat_id' => 'nullable|integer|required_with:eshop_id,eshop_parent_id',
        'eshop_parent_id' => 'nullable|integer|exists:eshops_category,eshop_cat_id',
      ])->validate();
    }
  }
}


?>
