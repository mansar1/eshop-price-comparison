<?php
namespace App\Actions;

class DeleteEshopProduct
{
  protected $getEshopProduct;

  public function __construct(GetEshopProduct $getEshopProduct)
  {
    $this->getEshopProduct = $getEshopProduct;
  }

  public function execute(int $id) : bool
  {
    // get product
    $product = $this->getEshopProduct->execute($id);

    return $product->delete();
  }
}


?>
