<?php
namespace App\Actions;

use Validator;
use App\Category;

class GetCategoryBranches
{
  protected $getCategoryBranch;

  public function __construct(GetCategoryBranch $getCategoryBranch)
  {
    $this->getCategoryBranch = $getCategoryBranch;
  }

  /**
   * Get branches of selected categories
   * @param  boolean $only_our get only manually created categories for our system
   * @param  integer $eshop_id
   * @return [type]            [description]
   */
  public function execute(array $data, int $eshop_id = -1) : object
  {
    // validate data
    $this->validate($data);

    // get primary categories
    if(isset($data['only_our']) && $data['only_our']){
      $categories = Category::whereNull('eshop_id')->whereNull('parent_id')->get();
    }
    else if($eshop_id > 0){
      $categories = Category::where('eshop_id', $eshop_id)->whereNull('eshop_parent_id')->get();
    }
    else{
      $categories = Category::
        where(function ($query) {
              $query->whereNotNull('eshop_id')
                    ->whereNull('eshop_parent_id');
          })
        ->orWhere(function ($query) {
              $query->whereNull('eshop_id')
                    ->whereNull('parent_id');
          })
        ->get();
    }

    if(!isset($data['only_primary']) || !$data['only_primary'])
    {
      foreach ($categories as $key => $category) {
        $category = $this->getCategoryBranch->execute($category, $data);
        if(isset($data['not_empty']) && $data['not_empty'] && $category->products_count < 1){
          $categories->forget($key);
        }
      }

      $categories = $categories->values();
    }

    return $categories;
  }

  private function validate(array $data)
  {
    Validator::make($data, [
        'only_our' => 'boolean',
        'only_primary' => 'boolean',
        'not_empty' => 'boolean',
        'only_vip' => 'boolean',
        'eshop_id' => 'integer',
      ])->validate();
  }
}


?>
