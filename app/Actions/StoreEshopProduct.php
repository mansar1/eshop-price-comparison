<?php
namespace App\Actions;

use Validator;
use App\EshopProduct;

class StoreEshopProduct
{
  protected $getEshopProduct;
  protected $findOriginalPrice;
  protected $storeProduct;

  public function __construct(GetEshopProduct $getEshopProduct, FindOriginalPrice $findOriginalPrice,
                              StoreProduct $storeProduct)
  {
    $this->getEshopProduct = $getEshopProduct;
    $this->findOriginalPrice = $findOriginalPrice;
    $this->storeProduct = $storeProduct;
  }

  public function execute(array $data) : EshopProduct
  {
    // validate data
    $this->validate($data);

    // get existing or create new product
    $product = isset($data['id']) ? $this->getEshopProduct->execute($data['id']) : new EshopProduct;

    // insert/update common product info
    if(isset($data['product']) && !empty($data['product'])){
      if(isset($data['product_id'])){
        $product->product_id = $data['product_id'];
        $this->storeProduct->execute((array) $data['product'], $data['product_id']);
      }
      else{
        $newProduct = $this->storeProduct->execute((array) $data['product']);
        $product->product_id = $newProduct['id'];
      }
    }
    else if(isset($data['product_id']))
      $product->product_id = $data['product_id'];

    if(isset($data['eshop_id']))
      $product->eshop_id = $data['eshop_id'];

    if(isset($data['url']))
      $product->url = $data['url'];

    if(isset($data['promotion_url']))
      $product->promotion_url = $data['promotion_url'];
    else if(array_key_exists('promotion_url', $data))
      $product->promotion_url = NULL;

    if(isset($data['rating_value']))
      $product->rating_value = $data['rating_value'];
    else if(array_key_exists('rating_value', $data))
      $product->rating_value = NULL;

    if(isset($data['rating_count']))
      $product->rating_count = $data['rating_count'];
    else if(array_key_exists('rating_count', $data))
      $product->rating_count = NULL;

    if(array_key_exists('current_price', $data))
    {
      $product->current_price = $data['current_price'];
      if(!$product->lowest_price || $product->current_price < $product->lowest_price)
        $product->lowest_price = $data['current_price'];
      if(!$product->highest_price || $product->current_price > $product->highest_price)
        $product->highest_price = $data['current_price'];

      // create or update existing history
      if($product->price_history){
        $history = $product->price_history;
        array_push($history, array("price" => $data['current_price'], "date" => date("Y-m-d H:i:s")));
        $history = serialize($history);
      }else{
        $history = array(array("price" => $data['current_price'], "date" => date("Y-m-d H:i:s")));
        $history = serialize($history);
      }

      $product->price_history = $history;

      // let's count more realistic original product price
      $product->original_price = $this->findOriginalPrice->execute(unserialize($history));
    }

    if(isset($data['sale_endtime']))
      $product->sale_endtime = $data['sale_endtime'];
    else if(array_key_exists('sale_endtime', $data))
      $product->sale_endtime = NULL;

    if(isset($data['price_change_time']))
      $product->price_change_time = $data['price_change_time'];

    $product->save();

    return $product;
  }

  private function validate(array $data)
  {
    if(isset($data['id']))
    {
      Validator::make($data, [
        'product_id' => 'integer|exists:eshops_product,id',
        'product' => 'array',
        'eshop_id' => 'integer|exists:eshops_eshop,id',
        'url' => 'string|min:3|max:255|nullable|url',
        'promotion_url' => 'string|min:3|max:255|nullable|url',
        'rating_value' => 'nullable|numeric',
        'rating_count' => 'nullable|integer',
        'current_price' => 'numeric',
      ])->validate();
    }
    else
    {
      Validator::make($data, [
        'product_id' => 'required_without:product|integer|exists:eshops_product,id',
        'product' => 'required_without:product_id|array',
        'eshop_id' => 'required|integer|exists:eshops_eshop,id',
        'url' => 'required|string|min:3|max:255|nullable|url',
        'promotion_url' => 'string|min:3|max:255|nullable|url',
        'rating_value' => 'nullable|numeric',
        'rating_count' => 'nullable|integer',
        'current_price' => 'required|numeric',
      ])->validate();
    }
  }
}


?>
