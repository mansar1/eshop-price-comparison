<?php
namespace App\Actions;

use App\Eshop;

class GetEshops
{
  public function execute(int $limit)
  {
    return  Eshop::paginate($limit);
  }
}


?>
