<?php
namespace App\Actions;

use App\Category;

class GetCategories
{
  public function execute(int $limit)
  {
    return  Category::paginate($limit);
  }
}


?>
