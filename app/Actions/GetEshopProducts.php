<?php
namespace App\Actions;

use Validator;
use App\EshopProduct;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class GetEshopProducts
{
  public function execute(array $data) : object
  {
    // validate data
    $this->validate($data);

    $category = null;
    $categoryIDS = null;

    if (isset($data['category_ids']) && $data['category_ids'] && count($data['category_ids']) > 0) {
      if(count($data['category_ids']) === 1){
        $category = $data['category_ids'][0];
      }
      else{
        $categoryIDS = $data['category_ids'];
      }
    }

    $price_from = isset($data['price_from']) ? $data['price_from'] : false;
    $price_to = isset($data['price_to']) ? $data['price_to'] : false;
    $limit = $data['limit'];
    $page = isset($data['page']) ? $data['page'] : 1;
    $order_by = isset($data['order_by']) ? $data['order_by'] : 'id';
    $order_type = isset($data['order_type']) ? $data['order_type'] : 'asc';
    $only_vip = isset($data['only_vip']) ? $data['only_vip'] : false;
    $eshop_id = isset($data['eshop_id']) ? $data['eshop_id'] : false;

    $sales_day_start = null;
    $sales_day_end = null;
    if (isset($data['only_sales']) && $data['only_sales']) {
      $only_sales = $data['only_sales'];
      if($only_sales == 'custom_day_sales'){
        $sales_day_start = $data['sales_day_start'];
        $sales_day_end = $data['sales_day_end'];
      }
    }else{
      $only_sales = false;
    }

    // Get filtered products
    $filteredProducts = EshopProduct::
      select('*', DB::raw('date(sale_endtime) as sale_endtime'),
             DB::raw('ROUND((current_price - lowest_price), 2) AS diff_lowest_price'),
             DB::raw('ROUND((original_price - current_price), 2) AS absolute_decrease'),
             DB::raw('ROUND((100 - current_price * 100 / original_price), 2) AS relative_decrease'))
     ->when($eshop_id, function ($q) use ($eshop_id) {
         return $q->where('eshop_id', '=', $eshop_id);
     })
     ->when($only_sales == 'custom_day_sales', function ($q) use ($sales_day_start, $sales_day_end) {
         return $q->whereRaw('sale_endtime > NOW()')
                  ->where('price_change_time', '>=', $sales_day_start)
                  ->where('price_change_time', '<=', $sales_day_end);
     })
     ->when($only_sales == 'yes', function ($q) {
         return $q->whereRaw('sale_endtime > NOW()');
     })
     ->when($price_from, function ($q) use ($price_from) {
         return $q->where('current_price', '>=', $price_from);
     })
     ->when($price_to, function ($q) use ($price_to) {
         return $q->where('current_price', '<=', $price_to);
     })
     ->whereHas('product', function ($query) use ($category, $categoryIDS, $only_vip) {
                $query->when(isset($category), function ($q) use ($category) {
                    return $q->where('category', $category);
                })
                ->when(isset($categoryIDS), function ($q) use ($categoryIDS) {
                    return $q->whereIn('category', $categoryIDS);
                })
                ->when($only_vip, function ($q) {
                    return $q->where('is_vip', true);
                });
            })
    ->with(['product',
           'coupons'])
    ->orderBy($order_by, $order_type)
    ->paginate($limit);

    return $filteredProducts;
  }

  private function validate(array $data)
  {
    Validator::make($data, [
      'category_ids' => 'array',
      'price_from' => 'numeric',
      'price_to' => 'numeric',
      'limit' => 'integer',
      'page' => 'integer',
      'order_by' => 'string',
      'order_type' => Rule::in(['asc', 'desc']),
      'eshop_id' => 'integer|exists:eshops_eshop,id',
      'only_vip' => 'boolean',
      'only_sales' => Rule::in(['yes', 'custom_day_sales']),
      'sales_day_start' => 'date',
      'sales_day_end' => 'date',
    ])->validate();
  }
}


?>
