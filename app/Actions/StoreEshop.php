<?php
namespace App\Actions;

use Validator;
use App\Eshop;

class StoreEshop
{
  protected $getEshop;

  public function __construct(GetEshop $getEshop)
  {
    $this->getEshop = $getEshop;
  }

  public function execute(array $data) : Eshop
  {
    // validate data
    $this->validate($data);

    // get existing or create new product
    $eshop = isset($data['id']) > 0 ? $this->getEshop->execute($data['id']) : new Eshop;

    if(isset($data['name']))
      $eshop->name = $data['name'];

    if(isset($data['url']))
      $eshop->url = $data['url'];

    if(isset($data['logo_url']))
      $eshop->logo_url = $data['logo_url'];
    else if(array_key_exists('logo_url', $data))
      $eshop->logo_url = NULL;

    $eshop->save();

    return $eshop;
  }

  private function validate(array $data)
  {
    if(isset($data['id']))
    {
      Validator::make($data, [
        'name' => 'string|min:3|max:255',
        'logo_url' => 'string|min:3|max:255|url|nullable',
        'url' => 'string|min:3|max:255|url',
      ])->validate();
    }
    else
    {
      Validator::make($data, [
        'name' => 'required|string|min:3|max:255',
        'logo_url' => 'string|min:3|max:255|nullable',
        'url' => 'required|string|min:3|max:255',
      ])->validate();
    }
  }
}


?>
