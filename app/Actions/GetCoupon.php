<?php
namespace App\Actions;

use App\Coupon;

class GetCoupon
{
  public function execute(int $id) : Coupon
  {
    return  Coupon::with(['eshop_product'])->findOrFail($id);
  }
}


?>
