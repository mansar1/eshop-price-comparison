<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'eshops_category';

  /**
  * Get the products for the category.
  */
  public function products()
  {
    return $this->hasMany('App\Product', 'category');
  }

  /**
  * Get the eshop that owns the category.
  */
  public function eshop()
  {
    return $this->belongsTo('App\Eshop', 'eshop_id');
  }

  /**
  * Get category parent.
  */
  public function parent()
  {
    return $this->belongsTo('App\Category', 'parent_id', 'id');
  }

  /**
  * Get children categories.
  */
  public function children()
  {
    return $this->hasMany('App\Category', 'parent_id', 'id');
  }

  /**
  * Get children eshop categories.
  */
  public function eshop_children()
  {
    return $this->hasMany('App\Category', 'eshop_parent_id', 'eshop_cat_id');
  }

  /**
  * Get eshop category parent.
  */
  public function eshop_parent()
  {
    return $this->belongsTo('App\Category', 'eshop_parent_id', 'eshop_cat_id');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($category) { // before delete() method call this
         $category->products()->each(function($product) {
            $product->delete();
         });
         $category->children()->each(function($child) {
            $child->delete();
         });
         $category->eshop_children()->each(function($child) {
            $child->delete();
         });
    });
  }
}
