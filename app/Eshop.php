<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eshop extends Model
{
  /**
  * The table associated with the model.
  *
  * @var string
  */
  protected $table = 'eshops_eshop';

  /**
  * Get the eshops products for the eshop.
  */
  public function eshops_products()
  {
    return $this->hasMany('App\EshopProduct', 'eshop_id');
  }

  /**
  * Get the categories for the eshop.
  */
  public function categories()
  {
    return $this->hasMany('App\Category', 'eshop_id');
  }

  public static function boot() {
    parent::boot();
    self::deleting(function($eshop) { // before delete() method call this
         $eshop->categories()->each(function($category) {
            $category->delete();
         });
         $eshop->eshops_products()->each(function($eshop_product) {
            $eshop_product->delete();
         });
    });
  }
}
