<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkedItem extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'eshops_marked_item';
}
