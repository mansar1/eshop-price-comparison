<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'eshops_coupon';

   /**
     * Get the eshop product that owns the coupon.
     */
    public function eshop_product()
    {
        return $this->belongsTo('App\EshopProduct', 'product_id');
    }
}
