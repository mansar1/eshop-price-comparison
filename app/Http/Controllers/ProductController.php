<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\Product as ProductResource;

use App\Actions\GetProducts;
use App\Actions\GetProduct;
use App\Actions\StoreProduct;
use App\Actions\DeleteProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int $limit
     * @return \Illuminate\Http\Response
     */
    public function index(int $limit = -1, GetProducts $getProducts)
    {
      $limit = $limit > 0 ? $limit : $this->limit;
      // Get products
      $products = $getProducts->execute($limit);

      // Return collection of products as a resource
      return ProductResource::collection($products);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, StoreProduct $storeProduct)
    {
      $data = $request->all();
      $data['id'] = $request->isMethod('put') ? $request->id : NULL;
      $product = $storeProduct->execute($data);

        return new ProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id, GetProduct $getProduct)
    {
      // get product
      $product = $getProduct->execute($id);

      return new ProductResource($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(int $id, DeleteProduct $deleteProduct)
     {
       if($deleteProduct->execute($id)){
         return response(null, 204);
       }
     }
}
