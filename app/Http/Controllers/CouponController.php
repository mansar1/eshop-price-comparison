<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\Coupon as CouponResource;

use App\Actions\GetCoupons;
use App\Actions\GetCoupon;
use App\Actions\StoreCoupon;
use App\Actions\DeleteCoupon;

class CouponController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  int $limit
   * @return \Illuminate\Http\Response
   */
  public function index(int $limit = -1, GetCoupons $getCoupons)
  {
    $limit = $limit > 0 ? $limit : $this->limit;
    // Get coupons
    $coupons = $getCoupons->execute($limit);

    // Return collection of coupons as a resource
    return CouponResource::collection($coupons);

  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, StoreCoupon $storeCoupon)
  {
    $data = $request->all();
    $data['id'] = $request->isMethod('put') ? $request->id : NULL;
    $coupon = $storeCoupon->execute($data);

    return new CouponResource($coupon);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(int $id, GetCoupon $getCoupon)
  {
    // get coupon
    $coupon = $getCoupon->execute($id);

    return new CouponResource($coupon);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function destroy(int $id, DeleteCoupon $deleteCoupon)
   {
     if($deleteCoupon->execute($id)){
       return response(null, 204);
     }
   }
}
