<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\Eshop as EshopResource;

use App\Actions\GetEshops;
use App\Actions\GetEshop;
use App\Actions\StoreEshop;
use App\Actions\DeleteEshop;

class EshopController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  int $limit
   * @param  GetEshops $getEshop
   * @return \Illuminate\Http\Response
   */
  public function index(int $limit = -1, GetEshops $getEshop)
  {
    $limit = $limit ? $limit : $this->limit;

    // Get eshops
    $eshops = $getEshops->execute($limit);

    // Return collection of eshops as a resource
    return EshopResource::collection($eshops);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
   public function store(Request $request, StoreEshop $storeEshop)
   {
     $data = $request->all();
     $data['id'] = $request->isMethod('put') ? $request->id : NULL;
     $eshop = $storeEshop->execute($data);

    return new EshopResource($eshop);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function show(int $id, GetEshop $getEshop)
   {
     // get eshop
    $eshop = $getEshop->execute($id);

    return new EshopResource($eshop);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
   public function destroy(int $id, DeleteEshop $deleteEshop)
   {
     if($deleteEshop->execute($id)){
       return response(null, 204);
     }
   }
}
