<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\EshopProduct as EshopProductResource;

use App\Actions\GetEshopProducts;
use App\Actions\GetEshopProduct;
use App\Actions\StoreEshopProduct;
use App\Actions\DeleteEshopProduct;

class EshopProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request, GetEshopProducts $getEshopProducts)
     {
       $data = $request->all();
       $data['limit'] = isset($data['limit']) ? $data['limit'] : $this->limit;
       $products = $getEshopProducts->execute($data);

      // Return collection of products as a resource
      return EshopProductResource::collection($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request, StoreEshopProduct $storeEshopProduct)
     {
       $data = $request->all();
       $data['id'] = $request->isMethod('put') ? $request->id : NULL;
       $product = $storeEshopProduct->execute($data);

      return new EshopProductResource($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(int $id, GetEshopProduct $getEshopProduct)
     {
       // get product
      $eshop_product = $getEshopProduct->execute($id);

      return new EshopProductResource($eshop_product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(int $id, DeleteEshopProduct $deleteEshopProduct)
     {
       if($deleteEshopProduct->execute($id)){
         return response(null, 204);
       }
     }
}
