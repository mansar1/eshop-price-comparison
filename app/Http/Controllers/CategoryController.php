<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Resources\Category as CategoryResource;

use App\Actions\GetCategories;
use App\Actions\GetCategory;
use App\Actions\StoreCategory;
use App\Actions\DeleteCategory;
use App\Actions\GetCategoryBranch;
use App\Actions\GetCategoryBranches;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int $limit
     * @param  GetCategories $getCategories
     * @return \Illuminate\Http\Response
     */
    public function index(int $limit = -1, GetCategories $getCategories)
    {
      $limit = $limit ? $limit : $this->limit;

      // Get categories
      $categories = $getCategories->execute($limit);

      // Return collection of categories as a resource
      return CategoryResource::collection($categories);

    }

    /**
     * Store a newly created resource in storage
     * @param  Request       $request
     * @param  StoreCategory $storeCategory  update or create new category
     * @return Response
     */
    public function store(Request $request, StoreCategory $storeCategory)
    {
        // create category branch (multiple categories)
        if($request->has('category_branch') && count($request->input('category_branch')) > 0){
          $categories = $request->input('category_branch');

          $newlyCreated = false;

          foreach ($categories as $category) {
            if($newlyCreated){
              $category['parent_id'] = $newlyCreated['id'];
            }
            $newlyCreated = $storeCategory->execute($category);
          }
          $category = $newlyCreated;
        }
        //single category creation
        else{
          $data = $request->all();
          $data['id'] = $request->isMethod('put') ? $request->id : NULL;
          $category = $storeCategory->execute($data);
        }

        return new CategoryResource($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  GetCategory   $getCategory  get existing category
     * @return \Illuminate\Http\Response
     */
    public function show(int $id, GetCategory $getCategory)
    {
      // get category
      $category = $getCategory->execute($id);

      return new CategoryResource($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  DeleteCategory   $deleteCategory  delete category
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id, DeleteCategory $deleteCategory)
    {
      if($deleteCategory->execute($id)){
        return response(null, 204);
      }
    }

    /**
     * Get category branch.
     *
     * @param  Request       $request
     * @param  GetCategoryBranch   $getCategoryBranch
     * @return \Illuminate\Http\Response
     */
    public function getBranch(Request $request, GetCategoryBranch $getCategoryBranch, GetCategory $getCategory)
    {
      // get category
      $category = $getCategory->execute($request->id);

      // get branch
      $categoryTree = $getCategoryBranch->execute($category, $request->all());

      return new CategoryResource($categoryTree);
    }

    /**
     * Get branches of categories
     *
     * @param  Request       $request
     * @param  GetCategoryBranches   $getCategoryBranches
     * @return \Illuminate\Http\Response
     */
    public function getBranches(Request $request, GetCategoryBranches $getCategoryBranches)
    {
      $eshop_id = isset($request->eshop_id) ? $request->eshop_id : -1;
      $categories = $getCategoryBranches->execute($request->all(), $eshop_id);

      return new CategoryResource($categories);
    }
}
