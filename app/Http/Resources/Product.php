<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Product extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      /*return [
          "id"                    => $this->id,
          "product_id"            => $this->product_id,
          "eshop_id"              => $this->eshop_id,
          "price_history_id"      => $this->price_history_id,
          "url"                   => $this->url,
          "promotion_url"         => $this->promotion_url,
          "rating_value"          => $this->rating_value,
          "rating_count"          => $this->rating_count,
          "price_history"         => $this->price_history,
          "current_price"         => $this->current_price,
          "lowest_price"          => $this->lowest_price,
          "highest_price"         => $this->highest_price,
          "sale_endtime"          => $this->sale_endtime,
          "price_change_time"     => $this->price_change_time,
          "created_at"            => $this->created_at,
          "updated_at"            => $this->updated_at,
          "name"                  => $this->name,
          "property"              => $this->property,
          "is_vip"                => $this->is_vip,
          "image_url"             => $this->image_url,
          "thumbnail_image_url"   => $this->thumbnail_image_url,
          "category"              => $this->category,
          "group_id"              => $this->group_id,
      ];*/
        return parent::toArray($request);
    }
}
