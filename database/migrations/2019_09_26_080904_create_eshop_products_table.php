<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEshopProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eshop_product', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('product_id')->unsigned();
          $table->integer('eshop_id')->unsigned();
          $table->string('url')->nullable();
          $table->string('promotion_url')->nullable();
          $table->double('rating_value', 2, 1)->nullable()->unsigned();
          $table->integer('rating_count')->nullable()->unsigned();
          $table->double('current_price', 2, 1)->unsigned();
          $table->double('original_price', 2, 1)->unsigned();
          $table->double('lowest_price', 2, 1)->unsigned();
          $table->double('highest_price', 2, 1)->unsigned();
          $table->text('price_history');
          $table->dateTime('sale_entime')->nullable();
          $table->dateTime('price_change_time');
          $table->timestamps();

          $table->unique('url');
          $table->unique('promotion_url');
          $table->foreign('product_id')->references('id')->on('product');
          $table->foreign('eshop_id')->references('id')->on('eshop');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eshop_product');
    }
}
