<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('product_id')->unsigned()->nullable();
          $table->integer('eshop_id')->unsigned()->nullable();
          $table->string('name');
          $table->text('description')->nullable();
          $table->string('type');
          $table->string('code');
          $table->string('url');
          $table->string('image_url')->nullable();
          $table->string('promotion_url')->nullable();
          $table->integer('category')->unsigned();
          $table->integer('max_limit')->unsigned()->nullable();
          $table->integer('current_used')->unsigned()->nullable();
          $table->double('discount', 11, 2);
          $table->enum('discount_type', ['percent', 'price']);
          $table->dateTime('valid_start');
          $table->dateTime('valid_end');
          $table->timestamps();

          $table->foreign('product_id')->references('id')->on('eshop_product');
          $table->foreign('eshop_id')->references('id')->on('eshop');
          $table->foreign('category')->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon');
    }
}
