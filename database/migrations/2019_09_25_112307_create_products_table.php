<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('property')->nullable();
            $table->integer('group_id')->unsigned();
            $table->string('image_url')->nullable();
            $table->string('thumbnail_image_url')->nullable();
            $table->integer('category')->unsigned();
            $table->double('rating_value', 2, 1)->nullable()->unsigned();
            $table->integer('rating_count')->nullable()->unsigned();
            $table->boolean('is_vip')->default(false);
            $table->timestamps();

            $table->index('is_vip');
            $table->foreign('category')->references('id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
