<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('eshop_id')->unsigned()->nullable();
            $table->integer('eshop_cat_id')->unsigned()->nullable();
            $table->integer('eshop_parent_id')->unsigned()->nullable();
            $table->timestamps();

            $table->unique('eshop_cat_id');
            $table->foreign('parent_id')->references('id')->on('category');
            $table->foreign('eshop_id')->references('id')->on('eshop');
            $table->foreign('eshop_parent_id')->references('eshop_cat_id')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
