<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

//Route::middleware('auth:api')->group(function () {
    // Create new product
    Route::post('product', 'ProductController@store');

    // Update product
    Route::put('product/{id}', 'ProductController@store');

    // Delete product
    Route::delete('product/{id}', 'ProductController@destroy');

    // Create new product
    Route::post('eshop_product', 'EshopProductController@store');

    // Update product
    Route::put('eshop_product/{id}', 'EshopProductController@store');

    // Delete product
    Route::delete('eshop_product/{id}', 'EshopProductController@destroy');

    // Create new category
    Route::post('category', 'CategoryController@store');

    // Update category
    Route::put('category/{id}', 'CategoryController@store');

    // Delete category
    Route::delete('category/{id}', 'CategoryController@destroy');

    // Create new coupon
    Route::post('coupon', 'CouponController@store');

    // Update coupon
    Route::put('coupon/{id}', 'CouponController@store');

    // Delete coupon
    Route::delete('coupon/{id}', 'CouponController@destroy');

    // Create new eshop
    Route::post('eshop', 'EshopController@store');

    // Update eshop
    Route::put('eshop/{id}', 'EshopController@store');

    // Delete eshop
    Route::delete('eshop/{id}', 'EshopController@destroy');
//});

// List products
Route::get('products', 'ProductController@index');

// List single product
Route::get('product/{id}', 'ProductController@show');

// List eshops products
Route::get('eshop_products/{limit?}', 'EshopProductController@index');

// List single product
Route::get('eshop_product/{id}', 'EshopProductController@show');

// Get branches of categories
Route::get('categories/branches/{eshop_id?}', 'CategoryController@getBranches');

// List categories
Route::get('categories/{limit?}', 'CategoryController@index');

// Get category branch
Route::get('category/{id}/branch', 'CategoryController@getBranch');

// List single category
Route::get('category/{id}', 'CategoryController@show');

// List coupons
Route::get('coupons/{limit?}', 'CouponController@index');

// List single coupon
Route::get('coupon/{id}', 'CouponController@show');

// List eshops
Route::get('eshops/{limit?}', 'EshopController@index');

// List single eshop
Route::get('eshop/{id}', 'EshopController@show');
